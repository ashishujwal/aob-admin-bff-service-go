
## Purpose

Small proof of concept for:
1. Spec first development for RestAPIs.
2. Go to be used as implementation language.
3. Document all the steps taken to develop the API.



## Dependencies

1. Docker should be installed.
2. Swagger should be installed.


## Commands

Validate a spec: 
swagger validate <path-to-swagger-yml>

Generate Go Server:
swagger generate server <path-to-swagger-yml>

## Packages generated

1. cmd/<service-name>/ - has **main.go**
2. models - Generated code for ** request / response ** objects.
3. restapi
    - operations : Boiler plate code for the different operations.

## FAQ

1. How do I plug in my own handlers ?
New handlers are typically **configured** in **configure.go** file.

2. What if the code is regenerated ? Would I loose my changes ?
If ** configure.go ** is present , then it is ** not regenerated ** again.
This means that the framework can be used quite conveniently if the swagger config
is not being modified to add new services. 

If a service is a micro-service then there should not be frequent interface additions.
If case new services are exposed through the same service code base, then some manual effort
would be required in configure.go so that the delta changes can be incorporated.

 

