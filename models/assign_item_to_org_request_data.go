// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// AssignItemToOrgRequestData assign item to org request data
// swagger:model AssignItemToOrgRequestData
type AssignItemToOrgRequestData struct {

	// item keys
	ItemKeys []string `json:"itemKeys"`

	// organizations
	Organizations []string `json:"organizations"`
}

// Validate validates this assign item to org request data
func (m *AssignItemToOrgRequestData) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateItemKeys(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateOrganizations(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *AssignItemToOrgRequestData) validateItemKeys(formats strfmt.Registry) error {

	if swag.IsZero(m.ItemKeys) { // not required
		return nil
	}

	return nil
}

func (m *AssignItemToOrgRequestData) validateOrganizations(formats strfmt.Registry) error {

	if swag.IsZero(m.Organizations) { // not required
		return nil
	}

	return nil
}

// MarshalBinary interface implementation
func (m *AssignItemToOrgRequestData) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *AssignItemToOrgRequestData) UnmarshalBinary(b []byte) error {
	var res AssignItemToOrgRequestData
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
