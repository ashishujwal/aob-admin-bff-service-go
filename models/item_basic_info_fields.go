// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// ItemBasicInfoFields item basic info fields
// swagger:model ItemBasicInfoFields
type ItemBasicInfoFields struct {

	// avg monthly usage
	AvgMonthlyUsage int64 `json:"avgMonthlyUsage,omitempty"`

	// item category
	ItemCategory string `json:"itemCategory,omitempty"`

	// item desc
	ItemDesc string `json:"itemDesc,omitempty"`

	// item no
	ItemNo string `json:"itemNo,omitempty"`

	// item short desc
	ItemShortDesc string `json:"itemShortDesc,omitempty"`

	// item sub status
	ItemSubStatus string `json:"itemSubStatus,omitempty"`

	// item type
	ItemType string `json:"itemType,omitempty"`

	// manufacturer name
	ManufacturerName string `json:"manufacturerName,omitempty"`

	// manufacturer pot number
	ManufacturerPotNumber string `json:"manufacturerPotNumber,omitempty"`

	// organization
	Organization string `json:"organization,omitempty"`

	// re order level
	ReOrderLevel string `json:"reOrderLevel,omitempty"`

	// re order quantity
	ReOrderQuantity int64 `json:"reOrderQuantity,omitempty"`
}

// Validate validates this item basic info fields
func (m *ItemBasicInfoFields) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *ItemBasicInfoFields) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ItemBasicInfoFields) UnmarshalBinary(b []byte) error {
	var res ItemBasicInfoFields
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
