// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// SupplierRowDataLinksViewSupplierInfo supplier row data links view supplier info
// swagger:model supplierRowDataLinksViewSupplierInfo
type SupplierRowDataLinksViewSupplierInfo struct {

	// href
	Href string `json:"href,omitempty"`

	// verb
	Verb string `json:"verb,omitempty"`
}

// Validate validates this supplier row data links view supplier info
func (m *SupplierRowDataLinksViewSupplierInfo) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *SupplierRowDataLinksViewSupplierInfo) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SupplierRowDataLinksViewSupplierInfo) UnmarshalBinary(b []byte) error {
	var res SupplierRowDataLinksViewSupplierInfo
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
