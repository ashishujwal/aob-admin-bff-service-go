// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// POAddPageLinksLinks p o add page links links
// swagger:model pOAddPageLinksLinks
type POAddPageLinksLinks struct {

	// search items
	SearchItems *POAddPageLinksLinksSearchItems `json:"searchItems,omitempty"`

	// search suppliers sites
	SearchSuppliersSites *POAddPageLinksLinksSearchSuppliersSites `json:"searchSuppliersSites,omitempty"`

	// submit p o
	SubmitPO *POAddPageLinksLinksSubmitPO `json:"submitPO,omitempty"`
}

// Validate validates this p o add page links links
func (m *POAddPageLinksLinks) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateSearchItems(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateSearchSuppliersSites(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateSubmitPO(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *POAddPageLinksLinks) validateSearchItems(formats strfmt.Registry) error {

	if swag.IsZero(m.SearchItems) { // not required
		return nil
	}

	if m.SearchItems != nil {

		if err := m.SearchItems.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("searchItems")
			}
			return err
		}

	}

	return nil
}

func (m *POAddPageLinksLinks) validateSearchSuppliersSites(formats strfmt.Registry) error {

	if swag.IsZero(m.SearchSuppliersSites) { // not required
		return nil
	}

	if m.SearchSuppliersSites != nil {

		if err := m.SearchSuppliersSites.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("searchSuppliersSites")
			}
			return err
		}

	}

	return nil
}

func (m *POAddPageLinksLinks) validateSubmitPO(formats strfmt.Registry) error {

	if swag.IsZero(m.SubmitPO) { // not required
		return nil
	}

	if m.SubmitPO != nil {

		if err := m.SubmitPO.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("submitPO")
			}
			return err
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *POAddPageLinksLinks) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *POAddPageLinksLinks) UnmarshalBinary(b []byte) error {
	var res POAddPageLinksLinks
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
