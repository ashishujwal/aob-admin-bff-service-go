// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// SupplierRowDataLinksViewHistory supplier row data links view history
// swagger:model supplierRowDataLinksViewHistory
type SupplierRowDataLinksViewHistory struct {

	// href
	Href string `json:"href,omitempty"`

	// verb
	Verb string `json:"verb,omitempty"`
}

// Validate validates this supplier row data links view history
func (m *SupplierRowDataLinksViewHistory) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *SupplierRowDataLinksViewHistory) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SupplierRowDataLinksViewHistory) UnmarshalBinary(b []byte) error {
	var res SupplierRowDataLinksViewHistory
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
