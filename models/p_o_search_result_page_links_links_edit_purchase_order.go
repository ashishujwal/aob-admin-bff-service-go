// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// POSearchResultPageLinksLinksEditPurchaseOrder p o search result page links links edit purchase order
// swagger:model pOSearchResultPageLinksLinksEditPurchaseOrder
type POSearchResultPageLinksLinksEditPurchaseOrder struct {

	// href
	Href string `json:"href,omitempty"`

	// verb
	Verb string `json:"verb,omitempty"`
}

// Validate validates this p o search result page links links edit purchase order
func (m *POSearchResultPageLinksLinksEditPurchaseOrder) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *POSearchResultPageLinksLinksEditPurchaseOrder) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *POSearchResultPageLinksLinksEditPurchaseOrder) UnmarshalBinary(b []byte) error {
	var res POSearchResultPageLinksLinksEditPurchaseOrder
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
