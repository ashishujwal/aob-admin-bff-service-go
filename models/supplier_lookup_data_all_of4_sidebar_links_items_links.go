// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// SupplierLookupDataAllOf4SidebarLinksItemsLinks supplier lookup data all of4 sidebar links items links
// swagger:model supplierLookupDataAllOf4SidebarLinksItemsLinks
type SupplierLookupDataAllOf4SidebarLinksItemsLinks struct {

	// request link
	RequestLink *SupplierLookupDataAllOf4SidebarLinksItemsLinksRequestLink `json:"requestLink,omitempty"`
}

// Validate validates this supplier lookup data all of4 sidebar links items links
func (m *SupplierLookupDataAllOf4SidebarLinksItemsLinks) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateRequestLink(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *SupplierLookupDataAllOf4SidebarLinksItemsLinks) validateRequestLink(formats strfmt.Registry) error {

	if swag.IsZero(m.RequestLink) { // not required
		return nil
	}

	if m.RequestLink != nil {

		if err := m.RequestLink.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("requestLink")
			}
			return err
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *SupplierLookupDataAllOf4SidebarLinksItemsLinks) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SupplierLookupDataAllOf4SidebarLinksItemsLinks) UnmarshalBinary(b []byte) error {
	var res SupplierLookupDataAllOf4SidebarLinksItemsLinks
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
