// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// SupplierBasicInfoLinksLinks supplier basic info links links
// swagger:model supplierBasicInfoLinksLinks
type SupplierBasicInfoLinksLinks struct {

	// save supplier basic info data
	SaveSupplierBasicInfoData *SupplierBasicInfoLinksLinksSaveSupplierBasicInfoData `json:"saveSupplierBasicInfoData,omitempty"`

	// state lookup by country
	StateLookupByCountry *SupplierBasicInfoLinksLinksStateLookupByCountry `json:"stateLookupByCountry,omitempty"`
}

// Validate validates this supplier basic info links links
func (m *SupplierBasicInfoLinksLinks) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateSaveSupplierBasicInfoData(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateStateLookupByCountry(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *SupplierBasicInfoLinksLinks) validateSaveSupplierBasicInfoData(formats strfmt.Registry) error {

	if swag.IsZero(m.SaveSupplierBasicInfoData) { // not required
		return nil
	}

	if m.SaveSupplierBasicInfoData != nil {

		if err := m.SaveSupplierBasicInfoData.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("saveSupplierBasicInfoData")
			}
			return err
		}

	}

	return nil
}

func (m *SupplierBasicInfoLinksLinks) validateStateLookupByCountry(formats strfmt.Registry) error {

	if swag.IsZero(m.StateLookupByCountry) { // not required
		return nil
	}

	if m.StateLookupByCountry != nil {

		if err := m.StateLookupByCountry.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("stateLookupByCountry")
			}
			return err
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *SupplierBasicInfoLinksLinks) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SupplierBasicInfoLinksLinks) UnmarshalBinary(b []byte) error {
	var res SupplierBasicInfoLinksLinks
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
