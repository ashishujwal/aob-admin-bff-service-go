// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// ItemRegistrationLinksLinks item registration links links
// swagger:model itemRegistrationLinksLinks
type ItemRegistrationLinksLinks struct {

	// cancel item registration
	CancelItemRegistration *ItemRegistrationLinksLinksCancelItemRegistration `json:"cancelItemRegistration,omitempty"`

	// item type lookup by itemcategory
	ItemTypeLookupByItemcategory *ItemRegistrationLinksLinksItemTypeLookupByItemcategory `json:"itemTypeLookupByItemcategory,omitempty"`

	// save item registration data
	SaveItemRegistrationData *ItemRegistrationLinksLinksSaveItemRegistrationData `json:"saveItemRegistrationData,omitempty"`
}

// Validate validates this item registration links links
func (m *ItemRegistrationLinksLinks) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCancelItemRegistration(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateItemTypeLookupByItemcategory(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if err := m.validateSaveItemRegistrationData(formats); err != nil {
		// prop
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *ItemRegistrationLinksLinks) validateCancelItemRegistration(formats strfmt.Registry) error {

	if swag.IsZero(m.CancelItemRegistration) { // not required
		return nil
	}

	if m.CancelItemRegistration != nil {

		if err := m.CancelItemRegistration.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("cancelItemRegistration")
			}
			return err
		}

	}

	return nil
}

func (m *ItemRegistrationLinksLinks) validateItemTypeLookupByItemcategory(formats strfmt.Registry) error {

	if swag.IsZero(m.ItemTypeLookupByItemcategory) { // not required
		return nil
	}

	if m.ItemTypeLookupByItemcategory != nil {

		if err := m.ItemTypeLookupByItemcategory.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("itemTypeLookupByItemcategory")
			}
			return err
		}

	}

	return nil
}

func (m *ItemRegistrationLinksLinks) validateSaveItemRegistrationData(formats strfmt.Registry) error {

	if swag.IsZero(m.SaveItemRegistrationData) { // not required
		return nil
	}

	if m.SaveItemRegistrationData != nil {

		if err := m.SaveItemRegistrationData.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("saveItemRegistrationData")
			}
			return err
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *ItemRegistrationLinksLinks) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ItemRegistrationLinksLinks) UnmarshalBinary(b []byte) error {
	var res ItemRegistrationLinksLinks
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
