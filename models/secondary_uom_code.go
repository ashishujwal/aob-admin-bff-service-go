// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// SecondaryUomCode secondary uom code
// swagger:model SecondaryUomCode
type SecondaryUomCode struct {

	// display text
	DisplayText string `json:"displayText,omitempty"`

	// value
	Value string `json:"value,omitempty"`
}

// Validate validates this secondary uom code
func (m *SecondaryUomCode) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *SecondaryUomCode) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *SecondaryUomCode) UnmarshalBinary(b []byte) error {
	var res SecondaryUomCode
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
