// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/tylerb/graceful"

	"aob/reference_data_go/restapi/operations"
	"aob/reference_data_go/restapi/operations/lookup"
	"aob/reference_data_go/restapi/operations/purchase_order"
	"aob/reference_data_go/domain"
)

//go:generate swagger generate server --target .. --name  --spec ../swagger.yml

func configureFlags(api *operations.AdminBffAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.AdminBffAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.AssignItemToOrgHandler = operations.AssignItemToOrgHandlerFunc(func(params operations.AssignItemToOrgParams) middleware.Responder {
		return middleware.NotImplemented("operation .AssignItemToOrg has not yet been implemented")
	})
	api.PurchaseOrderGetNewPoPageDataHandler = purchase_order.GetNewPoPageDataHandlerFunc(func(params purchase_order.GetNewPoPageDataParams) middleware.Responder {
		return middleware.NotImplemented("operation purchase_order.GetNewPoPageData has not yet been implemented")
	})
	api.PurchaseOrderGetSearchPageDataHandler = purchase_order.GetSearchPageDataHandlerFunc(func(params purchase_order.GetSearchPageDataParams) middleware.Responder {
		return middleware.NotImplemented("operation purchase_order.GetSearchPageData has not yet been implemented")
	})
	api.RetrieveBasicInfoHandler = operations.RetrieveBasicInfoHandlerFunc(func(params operations.RetrieveBasicInfoParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveBasicInfo has not yet been implemented")
	})
	api.RetrieveBasicInfoDataByIDHandler = operations.RetrieveBasicInfoDataByIDHandlerFunc(func(params operations.RetrieveBasicInfoDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveBasicInfoDataByID has not yet been implemented")
	})
	api.RetrieveBasicInfoEditDataByIDHandler = operations.RetrieveBasicInfoEditDataByIDHandlerFunc(func(params operations.RetrieveBasicInfoEditDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveBasicInfoEditDataByID has not yet been implemented")
	})
	api.RetrieveOrgDataByIDHandler = operations.RetrieveOrgDataByIDHandlerFunc(func(params operations.RetrieveOrgDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveOrgDataByID has not yet been implemented")
	})
	api.RetrieveOrgEditDataByIDHandler = operations.RetrieveOrgEditDataByIDHandlerFunc(func(params operations.RetrieveOrgEditDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveOrgEditDataByID has not yet been implemented")
	})
	api.RetrieveRegistrationPageDataHandler = operations.RetrieveRegistrationPageDataHandlerFunc(func(params operations.RetrieveRegistrationPageDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveRegistrationPageData has not yet been implemented")
	})
	api.RetrieveSearchPageDataHandler = operations.RetrieveSearchPageDataHandlerFunc(func(params operations.RetrieveSearchPageDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSearchPageData has not yet been implemented")
	})
	api.RetrieveSiteDataByIDHandler = operations.RetrieveSiteDataByIDHandlerFunc(func(params operations.RetrieveSiteDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSiteDataByID has not yet been implemented")
	})
	api.RetrieveSiteEditDataByIDHandler = operations.RetrieveSiteEditDataByIDHandlerFunc(func(params operations.RetrieveSiteEditDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSiteEditDataByID has not yet been implemented")
	})
	api.RetrieveSitePageDataHandler = operations.RetrieveSitePageDataHandlerFunc(func(params operations.RetrieveSitePageDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSitePageData has not yet been implemented")
	})
	api.RetrieveSiteViewDataByIDHandler = operations.RetrieveSiteViewDataByIDHandlerFunc(func(params operations.RetrieveSiteViewDataByIDParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSiteViewDataByID has not yet been implemented")
	})
	api.LookupRetrieveStatesHandler = lookup.RetrieveStatesHandlerFunc(func(params lookup.RetrieveStatesParams) middleware.Responder {
		return middleware.NotImplemented("operation lookup.RetrieveStates has not yet been implemented")
	})

	// Suppliers
	supplierDatabase := domain.NewSupplierDatabase()
	supplierService := domain.NewSupplierService(supplierDatabase)

	api.RetrieveSupplierLookupDataHandler = operations.RetrieveSupplierLookupDataHandlerFunc(func(params operations.RetrieveSupplierLookupDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .RetrieveSupplierLookupDataHandlerFunc has not yet been implemented")

	})

	api.SaveBasicInfoHandler = operations.SaveBasicInfoHandlerFunc(func(params operations.SaveBasicInfoParams) middleware.Responder {
		return middleware.NotImplemented("operation .SaveBasicInfo has not yet been implemented")
	})
	api.SaveItemRegistrationDataHandler = operations.SaveItemRegistrationDataHandlerFunc(func(params operations.SaveItemRegistrationDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .SaveItemRegistrationData has not yet been implemented")
	})
	api.SaveOrganizationDataHandler = operations.SaveOrganizationDataHandlerFunc(func(params operations.SaveOrganizationDataParams) middleware.Responder {
		return middleware.NotImplemented("operation .SaveOrganizationData has not yet been implemented")
	})
	api.PurchaseOrderSavePODataHandler = purchase_order.SavePODataHandlerFunc(func(params purchase_order.SavePODataParams) middleware.Responder {
		return middleware.NotImplemented("operation purchase_order.SavePOData has not yet been implemented")
	})
	api.SaveSiteInfoHandler = operations.SaveSiteInfoHandlerFunc(func(params operations.SaveSiteInfoParams) middleware.Responder {
		return middleware.NotImplemented("operation .SaveSiteInfo has not yet been implemented")
	})
	api.SearchForItemsHandler = operations.SearchForItemsHandlerFunc(func(params operations.SearchForItemsParams) middleware.Responder {
		return middleware.NotImplemented("operation .SearchForItems has not yet been implemented")
	})
	api.PurchaseOrderSearchPOHandler = purchase_order.SearchPOHandlerFunc(func(params purchase_order.SearchPOParams) middleware.Responder {
		return middleware.NotImplemented("operation purchase_order.SearchPO has not yet been implemented")
	})
	api.SearchSuppliersHandler = operations.SearchSuppliersHandlerFunc(func(params operations.SearchSuppliersParams) middleware.Responder {
		suppliers := supplierService.SearchSuppliers()
		return operations.NewSearchSuppliersOK().WithPayload(&suppliers)
	})
	api.UpdateSupplierBasicInfoHandler = operations.UpdateSupplierBasicInfoHandlerFunc(func(params operations.UpdateSupplierBasicInfoParams) middleware.Responder {
		return middleware.NotImplemented("operation .UpdateSupplierBasicInfo has not yet been implemented")
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *graceful.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
