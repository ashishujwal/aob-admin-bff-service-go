// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "aob/reference_data_go/models"
)

// RetrieveSiteViewDataByIDOKCode is the HTTP code returned for type RetrieveSiteViewDataByIDOK
const RetrieveSiteViewDataByIDOKCode int = 200

/*RetrieveSiteViewDataByIDOK Success

swagger:response retrieveSiteViewDataByIdOK
*/
type RetrieveSiteViewDataByIDOK struct {

	/*
	  In: Body
	*/
	Payload *models.SupplierOrgDataForEdit `json:"body,omitempty"`
}

// NewRetrieveSiteViewDataByIDOK creates RetrieveSiteViewDataByIDOK with default headers values
func NewRetrieveSiteViewDataByIDOK() *RetrieveSiteViewDataByIDOK {

	return &RetrieveSiteViewDataByIDOK{}
}

// WithPayload adds the payload to the retrieve site view data by Id o k response
func (o *RetrieveSiteViewDataByIDOK) WithPayload(payload *models.SupplierOrgDataForEdit) *RetrieveSiteViewDataByIDOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the retrieve site view data by Id o k response
func (o *RetrieveSiteViewDataByIDOK) SetPayload(payload *models.SupplierOrgDataForEdit) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *RetrieveSiteViewDataByIDOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

/*RetrieveSiteViewDataByIDDefault Error

swagger:response retrieveSiteViewDataByIdDefault
*/
type RetrieveSiteViewDataByIDDefault struct {
	_statusCode int
}

// NewRetrieveSiteViewDataByIDDefault creates RetrieveSiteViewDataByIDDefault with default headers values
func NewRetrieveSiteViewDataByIDDefault(code int) *RetrieveSiteViewDataByIDDefault {
	if code <= 0 {
		code = 500
	}

	return &RetrieveSiteViewDataByIDDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the retrieve site view data by Id default response
func (o *RetrieveSiteViewDataByIDDefault) WithStatusCode(code int) *RetrieveSiteViewDataByIDDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the retrieve site view data by Id default response
func (o *RetrieveSiteViewDataByIDDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WriteResponse to the client
func (o *RetrieveSiteViewDataByIDDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(o._statusCode)
}
