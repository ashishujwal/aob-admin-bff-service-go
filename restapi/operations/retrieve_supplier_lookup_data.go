// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// RetrieveSupplierLookupDataHandlerFunc turns a function with the right signature into a retrieve supplier lookup data handler
type RetrieveSupplierLookupDataHandlerFunc func(RetrieveSupplierLookupDataParams) middleware.Responder

// Handle executing the request and returning a response
func (fn RetrieveSupplierLookupDataHandlerFunc) Handle(params RetrieveSupplierLookupDataParams) middleware.Responder {
	return fn(params)
}

// RetrieveSupplierLookupDataHandler interface for that can handle valid retrieve supplier lookup data params
type RetrieveSupplierLookupDataHandler interface {
	Handle(RetrieveSupplierLookupDataParams) middleware.Responder
}

// NewRetrieveSupplierLookupData creates a new http.Handler for the retrieve supplier lookup data operation
func NewRetrieveSupplierLookupData(ctx *middleware.Context, handler RetrieveSupplierLookupDataHandler) *RetrieveSupplierLookupData {
	return &RetrieveSupplierLookupData{Context: ctx, Handler: handler}
}

/*RetrieveSupplierLookupData swagger:route GET /suppliers/lookup Supplier Lookup retrieveSupplierLookupData

Get lookup data for supplier page module

*/
type RetrieveSupplierLookupData struct {
	Context *middleware.Context
	Handler RetrieveSupplierLookupDataHandler
}

func (o *RetrieveSupplierLookupData) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewRetrieveSupplierLookupDataParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
