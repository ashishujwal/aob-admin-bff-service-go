// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "aob/reference_data_go/models"
)

// RetrieveSiteDataByIDOKCode is the HTTP code returned for type RetrieveSiteDataByIDOK
const RetrieveSiteDataByIDOKCode int = 200

/*RetrieveSiteDataByIDOK Success

swagger:response retrieveSiteDataByIdOK
*/
type RetrieveSiteDataByIDOK struct {

	/*
	  In: Body
	*/
	Payload *models.SupplierOrganizationViewData `json:"body,omitempty"`
}

// NewRetrieveSiteDataByIDOK creates RetrieveSiteDataByIDOK with default headers values
func NewRetrieveSiteDataByIDOK() *RetrieveSiteDataByIDOK {

	return &RetrieveSiteDataByIDOK{}
}

// WithPayload adds the payload to the retrieve site data by Id o k response
func (o *RetrieveSiteDataByIDOK) WithPayload(payload *models.SupplierOrganizationViewData) *RetrieveSiteDataByIDOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the retrieve site data by Id o k response
func (o *RetrieveSiteDataByIDOK) SetPayload(payload *models.SupplierOrganizationViewData) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *RetrieveSiteDataByIDOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

/*RetrieveSiteDataByIDDefault Error

swagger:response retrieveSiteDataByIdDefault
*/
type RetrieveSiteDataByIDDefault struct {
	_statusCode int
}

// NewRetrieveSiteDataByIDDefault creates RetrieveSiteDataByIDDefault with default headers values
func NewRetrieveSiteDataByIDDefault(code int) *RetrieveSiteDataByIDDefault {
	if code <= 0 {
		code = 500
	}

	return &RetrieveSiteDataByIDDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the retrieve site data by Id default response
func (o *RetrieveSiteDataByIDDefault) WithStatusCode(code int) *RetrieveSiteDataByIDDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the retrieve site data by Id default response
func (o *RetrieveSiteDataByIDDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WriteResponse to the client
func (o *RetrieveSiteDataByIDDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(o._statusCode)
}
