// Code generated by go-swagger; DO NOT EDIT.

package purchase_order

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "aob/reference_data_go/models"
)

// SearchPOOKCode is the HTTP code returned for type SearchPOOK
const SearchPOOKCode int = 200

/*SearchPOOK Success

swagger:response searchPOOK
*/
type SearchPOOK struct {

	/*
	  In: Body
	*/
	Payload *models.POSearchResultData `json:"body,omitempty"`
}

// NewSearchPOOK creates SearchPOOK with default headers values
func NewSearchPOOK() *SearchPOOK {

	return &SearchPOOK{}
}

// WithPayload adds the payload to the search p o o k response
func (o *SearchPOOK) WithPayload(payload *models.POSearchResultData) *SearchPOOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the search p o o k response
func (o *SearchPOOK) SetPayload(payload *models.POSearchResultData) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SearchPOOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

/*SearchPODefault Error

swagger:response searchPODefault
*/
type SearchPODefault struct {
	_statusCode int
}

// NewSearchPODefault creates SearchPODefault with default headers values
func NewSearchPODefault(code int) *SearchPODefault {
	if code <= 0 {
		code = 500
	}

	return &SearchPODefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the search p o default response
func (o *SearchPODefault) WithStatusCode(code int) *SearchPODefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the search p o default response
func (o *SearchPODefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WriteResponse to the client
func (o *SearchPODefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(o._statusCode)
}
