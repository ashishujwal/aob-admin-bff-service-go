// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// SearchSuppliersHandlerFunc turns a function with the right signature into a search suppliers handler
type SearchSuppliersHandlerFunc func(SearchSuppliersParams) middleware.Responder

// Handle executing the request and returning a response
func (fn SearchSuppliersHandlerFunc) Handle(params SearchSuppliersParams) middleware.Responder {
	return fn(params)
}

// SearchSuppliersHandler interface for that can handle valid search suppliers params
type SearchSuppliersHandler interface {
	Handle(SearchSuppliersParams) middleware.Responder
}

// NewSearchSuppliers creates a new http.Handler for the search suppliers operation
func NewSearchSuppliers(ctx *middleware.Context, handler SearchSuppliersHandler) *SearchSuppliers {
	return &SearchSuppliers{Context: ctx, Handler: handler}
}

/*SearchSuppliers swagger:route POST /suppliers/search Supplier Supplier Search searchSuppliers

Post supplier search data from ui to back-end

*/
type SearchSuppliers struct {
	Context *middleware.Context
	Handler SearchSuppliersHandler
}

func (o *SearchSuppliers) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewSearchSuppliersParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
