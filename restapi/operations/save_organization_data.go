// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// SaveOrganizationDataHandlerFunc turns a function with the right signature into a save organization data handler
type SaveOrganizationDataHandlerFunc func(SaveOrganizationDataParams) middleware.Responder

// Handle executing the request and returning a response
func (fn SaveOrganizationDataHandlerFunc) Handle(params SaveOrganizationDataParams) middleware.Responder {
	return fn(params)
}

// SaveOrganizationDataHandler interface for that can handle valid save organization data params
type SaveOrganizationDataHandler interface {
	Handle(SaveOrganizationDataParams) middleware.Responder
}

// NewSaveOrganizationData creates a new http.Handler for the save organization data operation
func NewSaveOrganizationData(ctx *middleware.Context, handler SaveOrganizationDataHandler) *SaveOrganizationData {
	return &SaveOrganizationData{Context: ctx, Handler: handler}
}

/*SaveOrganizationData swagger:route POST /suppliers/registration/organization Supplier SupplierOrganization saveOrganizationData

POST Organization data fields for supplier organization page

*/
type SaveOrganizationData struct {
	Context *middleware.Context
	Handler SaveOrganizationDataHandler
}

func (o *SaveOrganizationData) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewSaveOrganizationDataParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
