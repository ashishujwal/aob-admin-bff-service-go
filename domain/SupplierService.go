package domain

import (
	"aob/reference_data_go/models"
	"github.com/go-openapi/errors"
)

type SupplierService struct {
	database SupplierDatabase
}

func NewSupplierService(database SupplierDatabase) SupplierService {
	return SupplierService{database: database}
}

func (service SupplierService) SearchSuppliers() models.SupplierListData {
	data := service.database.SearchSupplier();
	results := make([] *models.SupplierRowData, len(data))
	i := 0
	for _, modelObject := range data {
		results[i] = mapModelToView(modelObject)
		i++
	}
	return models.SupplierListData{results}

}

func mapModelToView(model SupplierData) (*models.SupplierRowData) {
	return &models.SupplierRowData{
		SupplierNumber: model.SupplierNumber,
		ParentSupplier: model.ParentSupplier,
		Country:        model.Country,
		State:          model.State,
		City:           model.City,
		Name:           model.Name,
		TaxID:          model.TaxId,
	}
}

type SupplierDatabase struct {
	values map[string]SupplierData
}

func (c SupplierDatabase) AddSupplier(data SupplierData) error {
	c.values[data.SupplierNumber] = data
	return nil
}

func (c SupplierDatabase) SearchSupplier() ([] SupplierData) {
	v := make([] SupplierData, len(c.values))
	i := 0
	for _, value := range c.values {
		v[i] = value
		i++
	}
	return v
}

func (c SupplierDatabase) UpdateSupplier(data SupplierData) error {
	existingSupplier, ok := c.values[data.SupplierNumber]
	if (ok) {
		c.values[existingSupplier.SupplierNumber] = data
		return nil
	} else {
		return errors.NotFound("Supplier was not found")
	}
}

type SupplierData struct {
	Name           string
	SupplierNumber string
	ParentSupplier string
	TaxId          string
	City           string
	State          string
	Country        string
}

func NewSupplierDatabase() (SupplierDatabase) {
	var supplierMap = make(map[string]SupplierData)
	supplierMap["1"] = SupplierData{Name: "Ashish", SupplierNumber: "1", ParentSupplier: "AOB", City: "Jaipur", State: "Rajasthan", Country: "India", TaxId: "taxId"}
	supplierMap["2"] = SupplierData{Name: "Someone else", SupplierNumber: "2", ParentSupplier: "AOB", City: "Jaipur", State: "Rajasthan", Country: "India", TaxId: "taxId"}
	return SupplierDatabase{values: supplierMap}
}
